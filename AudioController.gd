extends Node
class_name AudioControllerScript

onready var LowPassFilter:AudioEffectFilter


func _ready():
	$BGM.set_volume_db(-80)
	$BGM.play()
	$BGMTween.interpolate_property($BGM, "volume_db", -80, -3, 2, 
									Tween.TRANS_EXPO, Tween.EASE_OUT)
	$BGMTween.start()
	

func music_danger():
	$BGMTween.interpolate_property($BGM, "volume_db", -3, -80, 3, 
									Tween.TRANS_EXPO, Tween.EASE_OUT)
	$BGMTween.start()
	$Stinger.set_volume_db(3)
	$Stinger.play()
	$Danger.play()
	$DangerTween.interpolate_property($Danger, "volume_db", -80, -3, 3, 
									Tween.TRANS_EXPO, Tween.EASE_OUT)
	$DangerTween.start()
	

func music_bgm():
	$DangerTween.interpolate_property($Danger, "volume_db", -3, -80, 3, 
									Tween.TRANS_EXPO, Tween.EASE_OUT)
	$DangerTween.start()
	$BGMTween.interpolate_property($BGM, "volume_db", -80, -3, 4, 
									Tween.TRANS_EXPO, Tween.EASE_OUT)
	$BGMTween.start()
	if ($Danger.get_volume_db() == -80):
		$Danger.stop()

func steps():
	$Steps.set_stream(load("res://assets/sfx/Steps/" + "step" + str(randi() %2) +".wav"))
	$Steps.set_pitch_scale(rand_range(1,1.7))
	$Steps.set_volume_db(rand_range(0,1))
	$Steps.play()
	
func shooting():
	$Shoot.set_pitch_scale(rand_range(1,1.9))
	$Shoot.set_volume_db(rand_range(-3,0))
	$Shoot.play()
	
func hit_damage():
	$Hit.set_pitch_scale(rand_range(1,2.1))
	$Hit.set_volume_db(rand_range(-3,0))
	$Hit.play()
	

	


