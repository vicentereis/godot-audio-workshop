extends KinematicBody2D

#Acessando funções e variáveis do controle de áudio
onready var AudioController = owner.get_tree().get_nodes_in_group("Audio")


func _ready():
	pass # Replace with function body.

	
func _on_Area2D_body_entered(body): 
	if (body.is_in_group("Player")):
		AudioController[0].music_danger()
		print("entrou")

func _on_Area2D_body_exited(body):
	if (body.is_in_group("Player")):
		AudioController[0].music_bgm()
		print("saiu")
		
