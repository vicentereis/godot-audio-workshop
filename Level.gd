extends Node2D


func _ready():
	pass
	

func _on_Area2D_body_entered(body):
	if (body.is_in_group("Player")):
		AudioServer.set_bus_send(AudioServer.get_bus_index("SFX"), "REVERB")

func _on_Area2D_body_exited(body):
	if (body.is_in_group("Player")):
		AudioServer.set_bus_send(AudioServer.get_bus_index("SFX"), "Master")

