extends KinematicBody2D

var initial_velocity = 1000
var velocity = Vector2(0,0)
var direction = Vector2()

func shoot(aim_position, gun_position):
	direction = (aim_position - gun_position).normalized()
	self.position = gun_position + direction*45
	self.rotation = direction.angle()
	velocity = direction * 1000

func _physics_process(delta):
	move_and_slide(velocity)

func _on_Area2D_body_entered(body):
	if body != self:
		if body.is_in_group("Enemy"):
			body.queue_free()
		self.queue_free()
